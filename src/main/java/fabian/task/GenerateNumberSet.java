package fabian.task;

import java.io.*;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class GenerateNumberSet {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj rozmiar zbioru tekstowego");
        int size=input.nextInt();
        System.out.println("Podaj lokalizacje pliku");
        String location=input.next();
        int[] array = new int[size];
        for(int i=0;i<size;i++){
            int randomNum = ThreadLocalRandom.current().nextInt(0, 100 + 1);
            array[i]=randomNum;
        }
        for(int i=0;i<size;i++) {
            try (FileWriter os = new FileWriter(location);
                 BufferedWriter bos = new BufferedWriter(os)) {
                bos.write(String.valueOf(array[i]));
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
