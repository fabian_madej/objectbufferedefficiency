package fabian.task;

import java.io.*;

public class ObjectBufferedEfficiency {
    public static void main(String[] args) {
        double t1, t2;
        t1=System.currentTimeMillis();
        for(int i=0;i<1024000;i++) {
            try (FileWriter os = new FileWriter("/tmp/output")) {
                os.write(String.valueOf(i));
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        t2=System.currentTimeMillis();
        System.out.println(t2-t1);
        t1=System.currentTimeMillis();
        for(int i=0;i<1024000;i++) {
            try (FileWriter os = new FileWriter("/tmp/test");
                 BufferedWriter bos = new BufferedWriter(os)) {
                bos.write(String.valueOf(i));
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
        t2=System.currentTimeMillis();
        System.out.println(t2-t1);
    }
}
