package fabian.task;

import java.io.*;
import java.util.Scanner;

public class ParseNumberSet {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj lokacje zbioru tekstowego");
        String location = input.next();
        try {
            Reader reader = new FileReader(location);
            char[] buffer = new char[1024];
            reader.read(buffer);
            System.out.println(buffer);
            reader.close();
        } catch (IOException ex) {
        }
    }
}
